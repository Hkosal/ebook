-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2021 at 08:25 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebook_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `order_item_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_item_name` varchar(250) NOT NULL,
  `order_item_quantity` int(11) NOT NULL,
  `order_item_price` double(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`order_item_id`, `order_id`, `order_item_name`, `order_item_quantity`, `order_item_price`) VALUES
(7, 4, 'How to teach people to become rich', 1, 3.75),
(8, 5, 'Millionaire Mind', 1, 4.00),
(9, 5, 'Mark Zuckerberg Billionaire', 1, 4.00),
(10, 6, 'How to teach people to become', 1, 3.75),
(11, 6, 'Rich Dad\'s Cashflow Quadrant', 1, 4.00),
(12, 7, 'How to teach people to become', 1, 3.75),
(13, 8, 'How to teach people to become', 2, 3.75),
(14, 8, 'Rich Dad\'s Cashflow Quadrant', 1, 4.00),
(15, 9, 'Rich Dad\'s Cashflow Quadrant', 1, 4.00),
(16, 9, 'How to teach people to become', 1, 3.75),
(17, 9, 'Millionaire Mind', 1, 4.00);

-- --------------------------------------------------------

--
-- Table structure for table `order_table`
--

CREATE TABLE `order_table` (
  `order_id` int(11) NOT NULL,
  `order_number` int(11) NOT NULL,
  `order_total_amount` double(12,2) NOT NULL,
  `transaction_id` varchar(200) NOT NULL,
  `card_cvc` int(5) NOT NULL,
  `card_expiry_month` varchar(30) NOT NULL,
  `card_expiry_year` varchar(30) NOT NULL,
  `order_status` varchar(50) NOT NULL,
  `card_holder_number` varchar(250) NOT NULL,
  `email_address` varchar(250) NOT NULL,
  `customer_name` varchar(250) NOT NULL,
  `customer_address` text NOT NULL,
  `customer_city` varchar(250) NOT NULL,
  `customer_pin` varchar(30) NOT NULL,
  `customer_state` varchar(250) NOT NULL,
  `customer_country` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_table`
--

INSERT INTO `order_table` (`order_id`, `order_number`, `order_total_amount`, `transaction_id`, `card_cvc`, `card_expiry_month`, `card_expiry_year`, `order_status`, `card_holder_number`, `email_address`, `customer_name`, `customer_address`, `customer_city`, `customer_pin`, `customer_state`, `customer_country`) VALUES
(4, 284673, 3.75, 'txn_3JSN8zK79rQ860W21fke5b3U', 123, '11', '2022', 'succeeded', '4242424242424242', 'hingsom@test.com', 'Hing Som', 'Phnom Penh', 'Phone Penh', '12000', '', 'Cambodia'),
(5, 152398, 8.00, 'txn_3JSNvDK79rQ860W20crcUdVG', 123, '12', '2022', 'succeeded', '4242424242424242', 'hingsom@test.com', 'Hing Som', 'Phnom Penh', 'Phone Penh', '12000', '', 'Cambodia'),
(6, 751779, 7.75, 'txn_3JSQlMK79rQ860W214kJN4Ac', 123, '12', '2022', 'succeeded', '4242424242424242', 'hingsom@test.com', 'Hing Som', 'Phnom Penh', 'Phone Penh', '12000', '', 'Cambodia'),
(7, 517329, 3.75, 'txn_3JSeqwK79rQ860W20qk4V0zf', 123, '12', '2022', 'succeeded', '4242424242424242', 'hingsom@test.com', 'Hing Som', 'Phnom Penh', 'Phone Penh', '12000', '', 'Cambodia'),
(8, 475648, 11.50, 'txn_3JShfyK79rQ860W20hnLN2FK', 123, '12', '2022', 'succeeded', '4242424242424242', 'hingsom@test.com', 'Hing Som', 'Phnom Penh', 'Phone Penh', '12000', '', 'Cambodia'),
(9, 946457, 11.75, 'txn_3JTWASK79rQ860W20wu1FphP', 232, '11', '2022', 'succeeded', '4242424242424242', 'somnang@test.com', 'Som Nang', 'Phnom Penh', 'Phone Penh', '12000', '', 'Cambodia');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`id`, `name`, `image`, `price`) VALUES
(26, 'How to teach people to become', 'code10.jpg', 3.75),
(27, 'Rich Dad\'s Cashflow Quadrant', 'code12.jpg', 4.00),
(28, 'Millionaire Mind', 'code17.jpg', 4.00),
(29, 'Mark Zuckerberg Billionaire', 'code19.jpg', 4.00),
(30, 'Smart Man', 'code21.jpg', 4.00),
(31, 'Gold Opportunity', 'code30.jpg', 3.75),
(32, 'The Power of Dream', 'code32.jpg', 4.00),
(33, 'Develop Human Resource', 'code34.jpg', 3.75);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `username`, `email`, `password`) VALUES
(1, 'Super', 'Admin', 'Super Admin', 'superadmin@gmail.com', '889a3a791b3875cfae413574b53da4bb8a90d53e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`order_item_id`);

--
-- Indexes for table `order_table`
--
ALTER TABLE `order_table`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `order_item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `order_table`
--
ALTER TABLE `order_table`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
