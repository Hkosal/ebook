<?php
//index.php
?>
<!DOCTYPE html>
<html>
<head>
    <title>EBook</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <style>
        .popover {
            width: 100%;
            max-width: 800px;
        }
        #navigation {
            text-align:center;
            background-color: #333;
            overflow:hidden;
            padding: 10px 0 10px 0;
        }
        #navigation ul {
            list-style-type: none;
            margin:auto;
            display:inline-block;
        }
        #navigation ul li {
            display: inline;
        }
        #navigation ul li a {
            padding: 14px 16px;
            color: white;
            text-align: center;
            text-decoration: none;
            font-size: 24px;
        }
        #navigation ul li a:hover {
            background-color: #111;
        }
    </style>
    <link rel="stylesheet" href="css/style.css">
</head>
 
<body>
	<div id="navigation">
		<ul>
			<li><a href="">Home</a></li>
			<li><a href="about.php">About</a></li>
			<li><a href="contactus.php">Contact us</a></li>
		</ul>
	</div>
    <div class="slidercontainer">  
        <div class="showSlide">  
            <img src="images/slide1.jpg" />  
            <div class="content"></div>  
        </div>  
        <div class="showSlide">  
            <img src="images/slide2.jpg"/>  
            <div class="content"></div>  
        </div>  
  
        <div class="showSlide">  
            <img src="images/slide3.jpg"/>  
            <div class="content"></div>  
        </div>  
        <div class="showSlide">  
            <img src="images/slide4.jpg"/>  
            <div class="content"></div>  
        </div>
        <!-- Navigation arrows -->  
        <a class="left" onclick="nextSlide(-1)">❮</a>  
        <a class="right" onclick="nextSlide(1)">❯</a>  
    </div>

<!--     SLIDESHOW USE BOOTSTRAP
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
    //Indicators
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    //Wrapper for slides
    <div class="carousel-inner">

      <div class="item active">
        <img src="images/slide1.jpg" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
          <h3>Los Angeles</h3>
          <p>LA is always so much fun!</p>
        </div>
      </div>

      <div class="item">
        <img src="images/slide2.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <h3>Chicago</h3>
          <p>Thank you, Chicago!</p>
        </div>
      </div>
    
      <div class="item">
        <img src="images/slide3.jpg" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <h3>New York</h3>
          <p>We love the Big Apple!</p>
        </div>
      </div>
  
    </div>

    //Left and right controls
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div> -->
    





    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Menu</span>
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                    </button>
                    <a class="navbar-brand" href="/">EBook</a>
                </div>
                <div id="navbar-cart" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a id="cart-popover" class="btn" data-placement="bottom" title="Shopping Cart">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                <span class="badge"></span>
                                <span class="total_price">$ 0.00</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
 
        <div id="popover_content_wrapper" style="display: none">
            <span id="cart_details"></span>
            <div align ="right">
                <a href="order_process.php" class="btn btn-primary" id="check_out_cart">
                    <span class="glyphicon glyphicon-shopping-cart"></span> Check out
                </a>
                <a href="#" class="btn btn-default" id="clear_cart">
                    <span class="glyphicon glyphicon-trash"></span> Clear
                </a>
            </div>
        </div>
 
        <div id="display_item" class="row">
 
        </div>
 
 
        <br />
        <br />
    </div>
    </div>
</body>
 
</html>
 
<script type="text/javascript">
    var slide_index = 1;  
        displaySlides(slide_index);  
  
        function nextSlide(n) {  
            displaySlides(slide_index += n);  
        }  
  
        function currentSlide(n) {  
            displaySlides(slide_index = n);  
        }  
  
        function displaySlides(n) {  
            var i;  
            var slides = document.getElementsByClassName("showSlide");  
            if (n > slides.length) { slide_index = 1 }  
            if (n < 1) { slide_index = slides.length }  
            for (i = 0; i < slides.length; i++) {  
                slides[i].style.display = "none";  
            }  
            slides[slide_index - 1].style.display = "block";  
        }

    $(document).ready(function() {
 
        load_product();
 
        load_cart_data();
 
        function load_product() {
            $.ajax({
                url: "fetch_item.php",
                method: "POST",
                success: function(data) {
                    $('#display_item').html(data);
                }
            })
        }
 
        function load_cart_data() {
            $.ajax({
                url: "fetch_cart.php",
                method: "POST",
                dataType: "json",
                success: function(data) {
                    $('#cart_details').html(data.cart_details);
                    $('.total_price').text(data.total_price);
                    $('.badge').text(data.total_item);
                }
            })
        }
 
        $('#cart-popover').popover({
            html: true,
            container: 'body',
            content: function() {
                return $('#popover_content_wrapper').html();
            }
        });
 
        $(document).on('click', '.add_to_cart', function() {
            var product_id = $(this).attr('id');
            var product_name = $('#name' + product_id + '').val();
            var product_price = $('#price' + product_id + '').val();
            var product_quantity = $('#quantity' + product_id).val();
            var action = 'add';
            if (product_quantity > 0) {
                $.ajax({
                    url: "action.php",
                    method: "POST",
                    data: {
                        product_id: product_id,
                        product_name: product_name,
                        product_price: product_price,
                        product_quantity: product_quantity,
                        action: action
                    },
                    success: function(data) {
                        load_cart_data();
                        alert("Item has been Added into Cart");
                    }
                })
            } else {
                alert("Please Enter Number of Quantity");
            }
        });
 
        $(document).on('click', '.delete', function() {
            var product_id = $(this).attr('id');
            var action = 'remove';
            if (confirm("Are you sure you want to remove this product?")) {
                $.ajax({
                    url: "action.php",
                    method: "POST",
                    data: {
                        product_id: product_id,
                        action: action
                    },
                    success: function(data) {
                        load_cart_data();
                        $('#cart-popover').popover('hide');
                        alert("Item has been removed from Cart");
                    }
                })
            } else {
                return false;
            }
        });
 
        $(document).on('click', '#clear_cart', function() {
            var action = 'empty';
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {
                    action: action
                },
                success: function() {
                    load_cart_data();
                    $('#cart-popover').popover('hide');
                    alert("Your Cart has been clear");
                }
            })
        });
 
    });
</script>