<!DOCTYPE html>
<html lang="en">
<head>
    <title>EBook</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    // social icon 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/contact.css"/>
    <style>
        .popover {
            width: 100%;
            max-width: 800px;
        }
        #navigation {
            text-align:center;
            background-color: #333;
            overflow:hidden;
            padding: 10px 0 10px 0;
        }
        #navigation ul {
            list-style-type: none;
            margin:auto;
            display:inline-block;
        }
        #navigation ul li {
            display: inline;
        }
        #navigation ul li a {
            padding: 14px 16px;
            color: white;
            text-align: center;
            text-decoration: none;
            font-size: 24px;
        }
        #navigation ul li a:hover {
            background-color: #111;
        }
    </style>
</head>
<body>
    <div id="navigation">
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="about.php">About</a></li>
			<li class="current"><a href="">Contact us</a></li>
		</ul>
	</div>
    <div style="justify-content: space-between; margin: 10px; margin-top: 50px;">
        <div class="col-12 col-sm-4">
            <div class="card bg-light mb-3">
                <div class="card-header bg-success text-white text-uppercase"><i class="fa fa-home"></i> Google Map</div>
                <div class="card-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3908.8492596546366!2d104.91853601425632!3d11.562661991790947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109513ed775f479%3A0xcac6753bebe2e9d2!2sUniversity%20of%20Puthisastra!5e0!3m2!1sen!2skh!4v1629909122084!5m2!1sen!2skh" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                </div>
            </div>
        </div>
        <section class="ftco-section">
            <div class="container">
                <!-- <div class="row justify-content-center">
                    <div class="col-md-6 text-center mb-5">
                        <h2 class="heading-section">Contact Us</h2>
                    </div>
                </div> -->
                <div class="row justify-content-center" style="padding-left: 120px;">
                    <div class="col-lg-10 col-md-12">
                        <div class="wrapper">
                            <div class="row no-gutters">
                                <div class="col-md-7 d-flex align-items-stretch">
                                    <div class="contact-wrap w-100 p-md-5 p-4">
                                        <h3 class="mb-4">Get in touch</h3>
                                        <div id="form-message-warning" class="mb-4"></div> 
                                <div id="form-message-success" class="mb-4">
                                Your message was sent, thank you!
                                </div>
                                        <h4 class="sent-notification"></h4>
                                        <form id="myForm" name="contactForm">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                                                    </div>
                                                </div>
                                                <div class="col-md-6"> 
                                                    <div class="form-group">
                                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="message" class="form-control" id="body" cols="30" rows="7" placeholder="Message"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <!-- <input type="submit" value="Send Message" class="btn btn-primary">
                                                        <div class="submitting"></div> -->
                                                        <button type="button" onclick="sendEmail()" value="Send Message" class="btn btn-primary">Send Message</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-5 d-flex align-items-stretch">
                                    <div class="info-wrap bg-primary w-100 p-lg-5 p-4">
                                        <h3 class="mb-4 mt-md-4">Contact us</h3>
                                <div class="dbox w-100 d-flex align-items-start">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-map-marker"></span>
                                    </div>
                                    <div class="text pl-3">
                                    <p><span>Address:</span> 198 West 21th Street, Suite 721 Phnom Penh</p>
                                </div>
                            </div>
                                <div class="dbox w-100 d-flex align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-phone"></span>
                                    </div>
                                    <div class="text pl-3">
                                    <p><span>Phone:</span> <a href="tel://1234567920">+855 969604049</a></p>
                                </div>
                            </div>
                                <div class="dbox w-100 d-flex align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-paper-plane"></span>
                                    </div>
                                    <div class="text pl-3">
                                    <p><span>Email:</span> <a href="mailto:hingkosalup@gmail.com">hingkosal@gmail.com</a></p>
                                </div>
                            </div>
                                <div class="dbox w-100 d-flex align-items-center">
                                    <div class="icon d-flex align-items-center justify-content-center">
                                        <span class="fa fa-globe"></span>
                                    </div>
                                    <div class="text pl-3">
                                    <p><span>Website</span> <a href="#">ebook.com</a></p>
                                </div>
                            </div>
                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- <center>
        <h4 class="sent-notification"></h4>
        <form id="myForm">
            <h2>Contact Us</h2>
            <label for="name">Name</label>
            <input id="name" type="text" placeholder="Enter Name">
            <br><br>
            <label>Email</label>
            <input id="email" type="text" placeholder="Enter Email">
            <br><br>
            <label>Subject</label>
            <input id="subject" type="text" placeholder="Enter Subject">
            <br><br>
            <label>Message</label>
            <textarea id="body" rows="5" placeholder="Type Message"></textarea>
            <br><br>
            <button type="button" onclick="sendEmail()" value="Send An Email">Submit</button>
        </form>
    </center> -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script type="text/javascript">
        function sendEmail() {
            var name = $("#name");
            var email = $("#email");
            var subject = $("#subject");
            var body = $("#body");

            if(isNotEmpty(name) && isNotEmpty(email) && isNotEmpty(subject) && isNotEmpty(body)) {
                $.ajax({
                    url: 'sendMail.php',
                    method: 'POST',
                    dataType: 'json',
                    data:{
                        name: name.val(),
                        email: email.val(),
                        subject: subject.val(),
                        body: body.val()
                    }, success: function(response){
                        $('#myForm')[0].reset();
                        $('.sent-notification').text("message send successfully.");
                    }
                });
            }
        }
        function isNotEmpty(caller) {
            if(caller.val()==""){
                caller.css('border', '1px solid red');
                return false;
            } else {
                caller.css('border', '');
                return true;
            }
        }
    </script>
</body>
</html>